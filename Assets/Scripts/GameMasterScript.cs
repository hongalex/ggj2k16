﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameMasterScript : MonoBehaviour {


    //INTRO 

    public float introTime = 9f; 


	public GameObject enemyholder;
	public PlayerScript player;

	//Transform child;
	List<GameObject> enemiesList;

	float alertTime = 5.0f;
	float timer;
	//float[] anglesArray = { 0.0f, 180.0f, 90.0f, 270.0f, 180.0f, 360.0f };
	float[] anglesArray = { 0.0f, 180.0f, 60.0f, 270.0f, 150.0f, 360.0f };

	void Start () {
		//Rotates based off parent's position
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>();
		enemiesList = new List<GameObject>();

		enemiesList.Add(GameObject.FindGameObjectWithTag("enemyholder"));
		enemiesList.Add((GameObject)Instantiate (enemyholder, transform.position, Quaternion.identity));

	
		timer = 40.0f;
	}


	void FixedUpdate() {
		timer -= Time.deltaTime;
        print(timer); 
	}

	public Light candle1, candle2, directional; 
    //public AudioSource 
	void Update() {

        //INTRO 
        if (timer < 30f)
        {
            candle1.enabled = false;
            candle2.enabled = false; 
			directional.enabled = false; 

		} 


		if(player.isAttacking())
		{
			int playerAngleIndex = getAngleIndex(player.getAttackDirection ());
			for(var i = 0; i < enemiesList.Count; i++)
			{
				EnemyScript enemy = enemiesList [i].transform.GetChild(0).GetComponent<EnemyScript> ();

				float orient = (enemy.getEnemyHolderOrientation () + 180) % 360;
				int enemyAngleIndex = getAngleIndex (orient);
				if (enemyAngleIndex == playerAngleIndex) {
					enemiesList.RemoveAt (i);
					enemy.die ();
					break;
				}
			}
			//Not attacking anymores
			player.setAttack (false);
		}
	} 

	public bool isAlertMode()
	{
		if (timer <= alertTime && timer > 0.0f)
			return true;
		return false;
	}

    public bool introMode()
    {
        if (timer > 30f)
        {
            return true; 
        } else
        {
            return false; 
        }
    }

	public bool isAttackMode()
	{
		if (timer <= 0.0f)
			return true;
		return false;
	}

	public float getTimer()
	{
		return timer;
	}

	//Checks for your index in the list of angles
	int getAngleIndex(float angle)
	{
		for (int i = 0; i < anglesArray.Length - 1; i+=2) {
			if (angle >= anglesArray [i] && angle < anglesArray [i + 1])
				return i;
		}

		return -1;
	}
}
