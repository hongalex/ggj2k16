﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {
	//Master script for timing
	public GameMasterScript master;

	//Sounds
	public float soundDelay;
	float oldSoundTime;
	public const int begSoundRange = 1;
	public const int endSoundRange = 3;
	public AudioSource footsteps; 

	//Initial settings
	public const int attackSpeed = 1;
	public int radius;

	//Moving properties
	float rotateSpeed;
	int direction;
	public float directionDelay;
	float oldDirectionTime;
	//Moving ranges
	public const int begDirRange = 1;
	public const int endDirRange = 3;
	public const float begRotateRange = 0.1f;
	public const float endRotateRange = 0.2f;

	//Check for wall occupancy
	bool hasHome = false;

	//Enemy holder
	public GameObject parent;

	// Use this for initialization
	void Start () {
		//Set location and facing direction
		Transform child = parent.transform.GetChild(0);
		//EnemyHolderScript parent = GetComponent<EnemyHolderScript> ();

		child.position = RandomOnUnitCircle (radius);
		child.LookAt(parent.transform);
		//Randomize rotation speed and sound

		rotateSpeed = Random.Range (begRotateRange, endRotateRange);
		soundDelay = Random.Range (begSoundRange, endSoundRange);

	}

	//Takes care of the wall being taken
	void OnTriggerStay(Collider other) {
		//Enemy collides with wall
		if (other.gameObject.tag == "wall" && master.isAlertMode ()) {
			if (other.GetComponent<WallScript> ().getResident () == null) { //If wall is currently empty 
				this.transform.position = other.transform.position; //Snap location
				other.GetComponent<WallScript> ().setResident (this.gameObject); //Set wall's resident as this enemy
				hasHome = true;
			}
		}
	} 

	//Lose phase
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject == parent.gameObject)
			print("You dead");
	}
		
	void FixedUpdate(){
		//Play and randomize sound
		if(oldSoundTime + soundDelay <= Time.time)
		{
			//this.GetComponent<AudioSource>().Play();
			oldSoundTime = Time.time;
			soundDelay = Random.Range (begSoundRange, endSoundRange);
		}

		//Randomize direction
		if (!master.isAlertMode ()) {
			if (oldDirectionTime + directionDelay <= Time.time) {
				randomizeDirection ();
				oldDirectionTime = Time.time;
				directionDelay = Random.Range (begDirRange, endDirRange);
			}
		}

        //States of the enemy
        //Attack
        if (master.isAttackMode()) {
            print("hello");
            transform.position = Vector3.MoveTowards(transform.position, parent.transform.position, attackSpeed * Time.deltaTime);
        }
        //Alert
        else if (master.isAlertMode()) {
            //No home, keep looking
            //print("alert");
            if (!hasHome)
                parent.transform.Rotate(new Vector3(0, rotateSpeed * direction, 0));
        } else  {
            //print("roate");
            footsteps.enabled = true; 
            parent.transform.Rotate(new Vector3 (0, rotateSpeed * direction, 0));
		}
	}

	// Update is called once per frame. Input
	void Update () {
		if (Input.GetKeyDown (KeyCode.D)) {
			Destroy (parent);
			//Destroy (this.gameObject);
		}
	}

	//Set random location
	public static Vector3 RandomOnUnitCircle( float radius) 
	{
		Vector2 point = Random.insideUnitCircle;
		point.Normalize();
		point *= radius;
		return new Vector3 (point.x, 0, point.y);
	}

	//Randomize direction
	public void randomizeDirection()
	{
		if (Random.value < 0.5f)
			direction = 1;
		else
			direction = -1;
	}


	public float getEnemyHolderOrientation()
	{
		return (parent.transform.eulerAngles.y ) % 360;
	}

	//Dies 
	public void die()
	{
		//Kills the enemy holder
		Destroy (parent.gameObject);
	}

	public void test()
	{
		print ("Testing");
	}
		
}

