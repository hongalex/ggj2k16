﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
    public Camera[] cameras;
    private int currentCam;
    public Transform CurrentMount;
    public float speed = 0.1f; 

	// Use this for initialization
	void Start () {
        currentCam = 0;
        //cameras[0].enabled = true; 

        for (int i = 1; i < cameras.Length; i++)
        {
            cameras[i].enabled = false; 
        }

        if (cameras.Length > 0)
        {
            cameras[0].enabled = true; 
        }
	}
	
	// Update is called once per frame
	void Update () {

        transform.position = Vector3.Lerp(transform.position, CurrentMount.position, speed);
        transform.rotation = Quaternion.Slerp(transform.rotation, CurrentMount.rotation, speed);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentCam++; 
            if (currentCam < cameras.Length)
            {
                cameras[currentCam - 1].enabled = false;
                cameras[currentCam].enabled = true;
            } else
            {
                cameras[currentCam - 1].enabled = false;
                currentCam = 0; 
                cameras[currentCam].enabled = true;
            }
        }
	}

    public void swingCam(Transform newMount)
    {
        CurrentMount = newMount; 
    }
}
