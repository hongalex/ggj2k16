﻿using UnityEngine;
using System.Collections;


public class WallScript : MonoBehaviour {
	public GameObject resident = null;

	//Get the enemy at the current wall
	public GameObject getResident()
	{
		return resident;
	}

	//Set the resident
	public void setResident(GameObject obj)
	{
		resident = obj; 
	}
}
