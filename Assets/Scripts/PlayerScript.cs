﻿using UnityEngine;
using System.Collections;
using Leap;

public class PlayerScript : MonoBehaviour {
	Controller controller;
	bool isAttack = false;
	float attackDir = 0.0f;

	void Start() {
		controller = new Controller ();
		controller.EnableGesture (Gesture.GestureType.TYPESWIPE);
		controller.Config.SetFloat ("Gesture.Swipe.MinLength", 100.0f);
		controller.Config.SetFloat ("Gesture.Swipe.MinVelocity", 750f);
		controller.EnableGesture (Gesture.GestureType.TYPESCREENTAP);
		controller.Config.SetFloat ("Gesture.ScreenTap.MinForwardVelocity", 30.0f);
		controller.Config.SetFloat ("Gesture.ScreenTap.HistorySeconds", .5f);
		controller.Config.SetFloat ("Gesture.ScreenTap.MinDistance", 1.0f);
		controller.EnableGesture (Gesture.GestureType.TYPECIRCLE);
		controller.Config.Save ();
	}

	//Returns facing direction
	public float getFacingDirection()
	{
		return transform.eulerAngles.y;
	}

	void Update () {
		Frame frame = controller.Frame ();
		GestureList gestures = frame.Gestures ();
		for (int i = 0; i < gestures.Count; i++) 
		{
			Gesture gesture = gestures [i];
			if (gesture.Type == Gesture.GestureType.TYPESWIPE) {
				Hand hand = frame.Hands.Frontmost;
				PointableList pb = hand.Pointables.Extended ();
				//changed to >=
				if (pb.Count >= 1) {
					SwipeGesture Swipe = new SwipeGesture (gesture);
					//adding this here
					this.GetComponent<AudioSource> ().Play ();
					isAttack = true;
					setAttackDirection ( (this.transform.parent).transform.eulerAngles.y);
				}
					/*
					Vector swipeDirection = Swipe.Direction;
					float x = swipeDirection.x;
					float y = swipeDirection.y;
					if (Mathf.Abs (x) > Mathf.Abs (y)) {
						this.GetComponent<AudioSource>().Play();
						isAttack = true;
						//Camera or this?
						setAttackDirection (this.transform.eulerAngles.y);
						/*
						if (x < 0) {
							//Swipe left
						} else if (x > 0) {
							//Swipe right;
						} */
					} /*else {
						if (y > 0) {
							//Swipe down;
						} else if (y < 0) {
							//Swipe up;
						}
					} 
				}	 
			} else if (gesture.Type == Gesture.GestureType.TYPECIRCLE) {
				//do stuff for circle gesture
			} */
		}
	}
	/*
	//Detect hands
	void Update()
	{
		
		//When it does a movement, grab the orientation and set attack direction
	}
	*/
	void setAttackDirection(float angle)
	{
		attackDir = angle;
	}

	//Master will use this and compare with enemy and destroy enemies based off of this
	public float getAttackDirection()
	{
		return attackDir;
	}

	public bool isAttacking()
	{
		return isAttack;
	}

	public void setAttack(bool isAtt)
	{
		isAttack = isAtt;
	}

}
