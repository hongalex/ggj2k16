﻿using UnityEngine;
using System.Collections;
using Leap;

public class HandGesture : MonoBehaviour {
	Controller controller;

	public GameObject block1;
	public GameObject block2;
	public GameObject block3;
	public GameObject block4;
	public GameObject block5;

	public GameObject sphere;
	//public Transform projectile;
	//private int count;
	//private ArrayList spheres;


	// Use this for initialization
	void Start () {
		controller = new Controller ();
		controller.EnableGesture (Gesture.GestureType.TYPESWIPE);
		controller.Config.SetFloat ("Gesture.Swipe.MinLength", 100.0f);
		controller.Config.SetFloat ("Gesture.Swipe.MinVelocity", 750f);
		controller.EnableGesture (Gesture.GestureType.TYPESCREENTAP);
		controller.Config.SetFloat ("Gesture.ScreenTap.MinForwardVelocity", 30.0f);
		controller.Config.SetFloat ("Gesture.ScreenTap.HistorySeconds", .5f);
		controller.Config.SetFloat ("Gesture.ScreenTap.MinDistance", 1.0f);
		controller.EnableGesture (Gesture.GestureType.TYPECIRCLE);
		controller.Config.Save ();

		//count = 0;
		//spheres = new ArrayList();
	}
	
	// Update is called once per frame
	void Update () {
		Frame frame = controller.Frame ();
		GestureList gestures = frame.Gestures ();
		for (int i = 0; i < gestures.Count; i++) 
		{
			Gesture gesture = gestures [i];
			if (gesture.Type == Gesture.GestureType.TYPESCREENTAP) {
				DestroyObject (block5);	
			} else if (gesture.Type == Gesture.GestureType.TYPESWIPE) {
				Hand hand = frame.Hands.Frontmost;
				PointableList pb = hand.Pointables.Extended ();
				if (pb.Count == 1) {
					SwipeGesture Swipe = new SwipeGesture (gesture);
					Vector swipeDirection = Swipe.Direction;
					float x = swipeDirection.x;
					float y = swipeDirection.y;
					if (Mathf.Abs (x) > Mathf.Abs (y)) {
						if (x < 0) {
							DestroyObject (block1);
						} else if (x > 0) {
							DestroyObject (block2);
						} 
					} else {
						if (y > 0) {
							DestroyObject (block3);
						} else if (y < 0) {
							DestroyObject (block4);
						}
					}
				}	 
			} else if (gesture.Type == Gesture.GestureType.TYPECIRCLE) {
				GameObject sphereClone;
				sphereClone = Instantiate (sphere, transform.position, transform.rotation) as GameObject;
				//sphereClone.GetComponent<Renderer> ().material.color = Random.ColorHSV (0f, 1f, 1f, 1f, 0.5f, 1f);

				//Transform bullet = Instantiate (projectile, transform.position, transform.rotation) as Transform ;
				//bullet.GetComponent<Rigidbody> ().AddForce(transform.forward*500); 

			}
		}
	
	}
}
